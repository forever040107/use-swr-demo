import axios from 'axios'

export default function fetcher({
  url,
  method = 'get',
  data = null,
  params = null,
}) {
  return axios({
    method,
    url,
    data,
    params,
    headers: {},
  })
}
