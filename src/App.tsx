import { useState } from 'react'
import useDataSWR from './hooks/useDataSWR'
import useMutation from './hooks/useMutation'
import { Table } from 'antd'

function App() {
  const [pageIndex, setPageIndex] = useState(1)

  const { data: resData } = useDataSWR(
    `https://626609f1dbee37aff9ab4953.mockapi.io/api/getList?p=${pageIndex}&l=10`
  )
  console.log('🚀 ~ file: App.tsx ~ line 7 ~ App ~ data', resData)

  const { mutate } = useMutation({
    url: 'https://626609f1dbee37aff9ab4953.mockapi.io/api/getList/37',
    method: 'DELETE',
  })

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'username',
      dataIndex: 'username',
      key: 'username',
    },
  ]

  const handleMutate = () => {
    mutate({
      onError: (error: any) => {
        console.log(
          '🚀 ~ file: App.tsx ~ line 32 ~ handleMutate ~ error',
          error
        )
        alert(error)
      },
      onSuccess: (res) => {
        if (res) {
          alert('ok')
        } else {
          alert('error')
        }
      },
    })
  }

  return (
    <div className="App">
      {resData && (
        <Table
          columns={columns}
          dataSource={resData && resData?.data}
          pagination={false}
        />
      )}
      <button type="button" onClick={() => handleMutate()}>
        delete me la!!
      </button>
      <br />
      <button type="button" onClick={() => setPageIndex(pageIndex - 1)}>
        previous page
      </button>
      <button type="button" onClick={() => setPageIndex(pageIndex + 1)}>
        next page
      </button>
    </div>
  )
}

export default App
