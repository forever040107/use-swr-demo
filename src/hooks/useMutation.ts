/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useState } from 'react'
import fetcher from '../libs/fetcher'

type mutationProps = {
  url: string
  method: string
}

type resEventProps = {
  onSuccess: (res: any) => void
  onError: (error: any) => void
}

export default function useMutation({ url, method = 'post' }: mutationProps) {
  const [isLoading, setIsLoading] = useState(false)

  const mutate = useCallback(({ onSuccess, onError }: resEventProps) => {
    async function execAsync() {
      try {
        setIsLoading(true)
        const res = await fetcher({
          url,
          method,
        })
        onSuccess(res)
      } catch (error) {
        if (error && onError) {
          onError(error)
        }
      } finally {
        setIsLoading(false)
      }
    }
    execAsync()
  }, [])

  return {
    mutate,
    isLoading,
  }
}
