import useSWR from 'swr'
import fetcher from '../libs/fetcher'

export default function useDataSWR(url: string, _fetcher = null) {
  const result = useSWR(
    url,
    _fetcher ? _fetcher : (url: string) => fetcher({ url })
  )

  return result
}
